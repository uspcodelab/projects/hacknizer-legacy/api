Rails.application.routes.draw do
  namespace :v1 do
    mount_devise_token_auth_for 'V1::User', at: 'auth'
    resources :hackathons
  end
end
