source 'https://rubygems.org'
ruby '~> 2.4'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.2'
# Use Puma as the app server
gem 'puma', '~> 3.6'
# Use PostgreSQL as the database for Active Record
gem 'pg', '~> 0.19'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 3.3'
# Use Redis adapter to set up a Redis-backed cache and/or session
gem 'redis-rails', '~> 5.0'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS),
# making cross-origin AJAX possible
gem 'rack-cors', '~> 0.4.0'

# Use DeviseTokenAuth to enable token based authorization
gem 'devise_token_auth', '~> 0.1.40'
# Use Omniauth for multiple-provider authentication
gem 'omniauth', '~> 1.6'

group :development do
  # Get notified of file changes. Read more: https://github.com/guard/listen
  gem 'listen', '~> 3.1'
end

group :development, :test, :lint do
  # Use Rubocop to check Ruby code style
  gem 'rubocop', '~> 0.46.0', require: false
  gem 'rubocop-rspec', '~> 1.9', require: false

  # Use Flay to detect code similarities
  gem 'flay', '~> 2.8', require: false

  # Use Reek to detect code smells
  gem 'reek', '~> 4.5', require: false

  # Use Brakeman to detect security vulnerabilities
  gem 'brakeman', '~> 3.4', require: false

  # Use Rails Best Practives to find bad Rails code
  gem 'rails_best_practices', '~> 1.17', require: false
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution
  # and get a debugger console
  gem 'byebug', '~> 9.0', platform: :mri

  # Use Database cleaner
  gem 'database_cleaner', '~> 1.5'

  # Use RSpec to create unit tests
  gem 'rspec-rails', '~> 3.5'

  # Use FactoryGirl to create mocks
  gem 'factory_girl_rails', '~> 4.8'

  # Use FFaker to generate dummy data
  gem 'ffaker', '~> 2.4'

  # Use Spring to preload rails application and speed up execution of tests
  gem 'spring', '~> 2.0'
  gem 'spring-watcher-listen', '~> 2.0'
  gem 'spring-commands-rspec', '~> 1.0'

  # Use SimpleCov to collect code coverage
  gem 'simplecov', '~> 0.12.0', require: false
end

group :test do
  # Create one-liner tests easily
  gem 'shoulda-matchers', '~> 3.1'

  # Test emails easily
  gem 'email_spec', '~> 2.1'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
