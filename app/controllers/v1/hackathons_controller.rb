class V1::HackathonsController < ApplicationController
  before_action :set_v1_hackathon, only: [:show, :update, :destroy]

  # GET /v1/hackathons
  def index
    @v1_hackathons = V1::Hackathon.all

    render json: @v1_hackathons
  end

  # GET /v1/hackathons/1
  def show
    render json: @v1_hackathon
  end

  # POST /v1/hackathons
  def create
    @v1_hackathon = V1::Hackathon.new(v1_hackathon_params)

    if @v1_hackathon.save
      render json: @v1_hackathon, status: :created, location: @v1_hackathon
    else
      render json: @v1_hackathon.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /v1/hackathons/1
  def update
    if @v1_hackathon.update(v1_hackathon_params)
      render json: @v1_hackathon
    else
      render json: @v1_hackathon.errors, status: :unprocessable_entity
    end
  end

  # DELETE /v1/hackathons/1
  def destroy
    @v1_hackathon.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_v1_hackathon
      @v1_hackathon = V1::Hackathon.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def v1_hackathon_params
      params.require(:v1_hackathon).permit(:path, :name, :description, :start, :end)
    end
end
