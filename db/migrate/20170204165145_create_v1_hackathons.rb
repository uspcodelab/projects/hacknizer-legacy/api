class CreateV1Hackathons < ActiveRecord::Migration[5.0]
  def change
    create_table :v1_hackathons do |t|
      t.string :path
      t.string :name
      t.text :description
      t.timestamp :start
      t.timestamp :end

      t.timestamps
    end
  end
end
