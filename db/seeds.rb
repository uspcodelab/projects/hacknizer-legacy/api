# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

V1::Hackathon.create([
  {
    path: 'triwizard-tournment-1994',
    name: 'Triwizard Tournment',
    description: 'A magical contest held between the three largest wizarding schools of Europe.',
    start: Time.zone.parse('1994-10-31T20:00:00Z'),
    end: Time.zone.parse('1995-06-24T20:00:00Z')
  },
  {
    path: 'hunger-games-74',
    name: '74th Hunger Games',
    description: 'A competition to remember the rebellion that once threatened Panem.',
    start: Time.zone.parse('2197-08-01T20:00:00Z'),
    end: Time.zone.parse('2197-08-13T20:00:00Z')
  }
])

V1::User.create([
  {
    email: 'harry.potter@hogwarts.ac.uk',
    nickname: 'hp',
    name: 'Harry Potter',
    password: 'voldemortsucks'
  }
])
