FROM ruby:2.4-slim
# Docker images can start off with nothing, but it's extremely
# common to pull in from a base image. In our case we're pulling
# in from the slim version of the official Ruby image.
#
# Details about this image can be found here:
# https://hub.docker.com/_/ruby/

LABEL com.hacknizer.maintainer="renato.cferreira@hotmail.com"
# It is good practice to set metadata for all of your Docker images.
# It's not necessary but it's a good habit.

RUN apt-get update && apt-get install -qq -y --no-install-recommends \
      build-essential libpq-dev
# Ensure that our apt package list is updated and install a package
# to ensure that we can communicate with PostgreSQL (libpq-dev).

ENV INSTALL_PATH /hacknizer
# The name of the application is hacknizer and while there
# is no standard on where your project should live inside of the
# Docker image, I like to put it in the root of the image and name
# it after the project.

RUN mkdir -p $INSTALL_PATH
# This just creates the folder in the Docker image at the
# install path we defined above.

WORKDIR $INSTALL_PATH
# We're going to be executing a number of commands below, and
# having to CD into the /hacknizer folder every time would
# be lame, so instead we can set the WORKDIR to be /hacknizer.
#
# By doing this, Docker will be smart enough to execute all
# future commands from within this directory.

COPY Gemfile Gemfile.lock ./
# This is going to copy in the Gemfile and Gemfile.lock from
# our work station at a path relative to the Dockerfile to
# the hacknizer/ path inside of the Docker image.
#
# It copies it to /hacknizer because of the WORKDIR being set.
#
# We copy in our Gemfile before the main app because Docker is
# smart enough to cache "layers" when you build a Docker image.
#
# This is an advanced concept but it means that we'll be able to
# cache all of our gems so that if we make an application code
# change, it won't re-run bundle install unless a gem changed.

RUN bundle install
# Install all gems inside the image to make all dependencies
# readly available when creating a container.

COPY . .
# This might look a bit alien but it's copying in everything from
# the current directory relative to the Dockerfile, over to the
# /hacknizer folder inside of the Docker image.
#
# We can get away with using the . for the second argument because
# this is how the unix command cp (copy) works. It stands for the
# current directory.

CMD puma -C config/puma.rb
# This is the command that's going to be ran by default if you run the
# Docker image without any arguments.
#
# In our case, it will start the Puma app server while passing in
# its config file.
