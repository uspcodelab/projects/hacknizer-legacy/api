require "rails_helper"

RSpec.describe V1::HackathonsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/v1/hackathons").to route_to("v1/hackathons#index")
    end

    it "routes to #show" do
      expect(:get => "/v1/hackathons/1").to route_to("v1/hackathons#show", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/v1/hackathons").to route_to("v1/hackathons#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/v1/hackathons/1").to route_to("v1/hackathons#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/v1/hackathons/1").to route_to("v1/hackathons#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/v1/hackathons/1").to route_to("v1/hackathons#destroy", :id => "1")
    end

  end
end
